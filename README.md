# libmake

## home, sweet home

    git@gitlab.com:m5658/libmake.git
    
## adopt me

    git subtree add  --message 'mtools git subtree add locator - libmake\nrepo=git@gitlab.com:m5658/libmake.git\nprefix=libmake\nbranch=main' --prefix libmake  git@gitlab.com:m5658/libmake.git main --squash

    echo include libmake/Makefile > Makefile
    git add Makefile
    git commit -m 'default Makefile from libmake' Makefile 


## inherit updates from the mother ship

    git subtree pull --prefix libmake git@gitlab.com:m5658/libmake.git main --squash

## push our updates to the mother ship

    git subtree push --prefix libmake git@gitlab.com:m5658/libmake.git main


